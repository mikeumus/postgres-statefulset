#!/bin/bash

cd config
. ./create_configmap.sh
cd ..

kubectl apply -f ./config/pql-secret.yml
kubectl apply -f ./config/hauser-secret.yml
kubectl apply -f ./service.yml
kubectl apply -f ./statefulset-master.yml